<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.1" name="tmw_desert_spacing" tilewidth="32" tileheight="32" spacing="1" margin="1" tilecount="48" columns="8">
 <image source="tmw_desert_spacing.png" width="265" height="199"/>
 <terraintypes>
  <terrain name="Nuevo Terreno" tile="1"/>
  <terrain name="PRUEBA" tile="9"/>
 </terraintypes>
 <tile id="0" terrain=",,,1"/>
 <tile id="1" terrain=",,1,1"/>
 <tile id="2" terrain=",,1,"/>
 <tile id="8" terrain=",1,,1"/>
 <tile id="9" terrain="1,1,1,1">
  <objectgroup draworder="index">
   <object id="5" x="5.875" y="11"/>
  </objectgroup>
 </tile>
 <tile id="10" terrain="1,,1,"/>
 <tile id="16" terrain=",1,,"/>
 <tile id="17" terrain="1,1,,"/>
 <tile id="18" terrain="1,,,"/>
 <tile id="19" terrain="1,1,1,"/>
 <tile id="20" terrain="1,1,,1"/>
 <tile id="27" terrain="1,,1,1"/>
 <tile id="28" terrain=",1,1,1"/>
</tileset>
