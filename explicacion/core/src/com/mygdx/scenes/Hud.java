package com.mygdx.scenes;

import javax.swing.ImageIcon;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.managers.ResourceManager;

public class Hud implements Disposable{

    public Stage stage;
    private Viewport viewport;

    private Integer tiempo;
    private boolean timeUp; 
    private float timeCount;
    private static Integer puntuacion;

 
    private static Label etValorPuntuacion;
    private static Label etPuntuacion;
    private Label etValorTiempo;
    private Label etTiempo;
    private Label etImagen;
    private Label etMoneda;
    
    private Image imagenPintar;
    ResourceManager resourceManager;
    
 

    public Hud(SpriteBatch sb){
    	resourceManager = new ResourceManager();
        //define our tracking variables
        tiempo = 80;
        timeCount = 0;
        puntuacion = 0;

        viewport = new FitViewport(1024,800,new OrthographicCamera());
        stage = new Stage(viewport, sb);
        // creamos una tabla para insertar lo que queremos ver en el hud
        Table table = new Table();
        table.top();
        table.setFillParent(true);
        // etiquetas que se iran modificando los valores
       etValorTiempo = new Label(String.format("%03d", tiempo), new Label.LabelStyle(new BitmapFont(), Color.WHITE)); // el %03 es para que aparezcan 3 digitos
       etValorPuntuacion =new Label(String.format("%06d", puntuacion), new Label.LabelStyle(new BitmapFont(), Color.WHITE)); // el %06 es para que aparezcan 6 digitos
       // etiquetas de la parte superior
       etTiempo = new Label("TIEMPO", new Label.LabelStyle(new BitmapFont(), Color.WHITE));
       etPuntuacion = new Label("PUNTUACION", new Label.LabelStyle(new BitmapFont(), Color.WHITE));
       etMoneda = new Label("MONEDA", new Label.LabelStyle(new BitmapFont(), Color.WHITE));
       imagenPintar = new Image(resourceManager.getTexture("bitcoin"));
      
       	table.add(etPuntuacion).expandX().padTop(10);
        table.add(etTiempo).expandX().padTop(10);
        table.add(etMoneda).expandX().padTop(10);
        table.row();
        table.add(etValorPuntuacion).expandX();
        table.add(etValorTiempo).expandX();
        table.add(imagenPintar).expandX();

      
        stage.addActor(table);

    }

    public void update(float dt){
        timeCount += dt;
        if(timeCount >= 1){
            if (tiempo > 0) {
                tiempo--;
            } else {
                timeUp = true;
            }
            timeCount = 0;
        }
    }

    public static void addScore(int value){
        puntuacion += value;
        etPuntuacion.setText(String.format("%06d", puntuacion));
    }

    @Override
    public void dispose() { stage.dispose(); }

    public boolean isTimeUp() { return timeUp; }

	
    
    
    
}
