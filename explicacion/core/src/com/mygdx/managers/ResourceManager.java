package com.mygdx.managers;



import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;


public class ResourceManager {
	private static Map<String, Texture> textures = new HashMap<String, Texture>();
	
	public static AssetManager assets = new AssetManager();
	
	public static void loadAllResources() {
		assets.load("music/cancion.mp3",Music.class);
		assets.load("characters/Paco.pack", TextureAtlas.class);
		ResourceManager.loadResource("bitcoin", new Texture("ui/bitcoin.png"));
	}
	public static void loadResource(String name, Texture resource) {
		textures.put(name, resource);
	}
	
	public static boolean update() {
		return assets.update();
	}
	
	public static TextureAtlas getAtlas(String path) {
		return assets.get(path, TextureAtlas.class);
	}
	
	public static Music getMusic(String path) {
		return assets.get(path,Music.class);	
	}

	public static void dispose() {
		assets.dispose();
	}
	public static Texture getTexture(String name) {
		return textures.get(name);
	}
}
