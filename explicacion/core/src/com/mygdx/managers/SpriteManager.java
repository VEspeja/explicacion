package com.mygdx.managers;

import javax.rmi.CORBA.Util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.particles.emitters.RegularEmitter.EmissionMode;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Plane.PlaneSide;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.mygdx.characters.Player;
import com.mygdx.game.Juego;


public class SpriteManager {
	public Juego game;
	private Batch batch;
	public OrthographicCamera camera;
	TiledMap map;
	OrthogonalTiledMapRenderer mapRenderer;
	
	
	public Player player;
	//public Array<Platform> platforms;
	public Array<RectangleMapObject> platforms;
	public Music music;
	RectangleMapObject rectangleObject ;
	
	public SpriteManager(Juego game) {
		this.game = game;
		batch = new SpriteBatch();
		camera = new OrthographicCamera();
		camera.setToOrtho(false,1024,600); // modificar por parametros
		//platforms = new Array<Platform>();
		platforms = new Array<RectangleMapObject>();
		
		loadCurrentLevel();
	}

	public void loadCurrentLevel() {
		player = new Player(this);
		player.position.set(10,100);
		map = new TmxMapLoader().load("levels/mapa.tmx");
		mapRenderer = new OrthogonalTiledMapRenderer(map);
		batch = mapRenderer.getBatch();
		mapRenderer.setView(camera);
		
		music = ResourceManager.getMusic("music/cancion.mp3");
		// PARA QUE LA MUSICA SE REPITA
		music.setLooping(true);
		// Para que suene
		music.play();
		
		
		MapLayer collisionsLayer = map.getLayers().get("colision");
		for (MapObject object : collisionsLayer.getObjects()) {
		   rectangleObject = (RectangleMapObject) object;
		   platforms.add(rectangleObject);
		}
		
	}
	
	public void update(float dt) {
		handleInput();
		player.update(dt);
		// camara que sigue al personaje
				camera.position.x = player.position.x;
				camera.position.y = player.position.y;
		camera.update();
	}
	
	public void draw() {
		mapRenderer.render(new int[]{0,1});
		batch.setProjectionMatrix(camera.combined);// ordenar al que pinta que proyecte las posiciones en pantalla
		batch.begin();
		player.render(batch);
		/*for (Platform platform : platforms) {
			platform.render(batch);
		}
		*/
		// una vez hecho todo, end
		batch.end();
		
		
	}
	private void handleInput() {
		if(Gdx.input.isKeyPressed(Keys.CONTROL_LEFT)|| Gdx.input.isKeyPressed(Keys.UP)) {
			if(player.canJump) {
				player.jump();
			}
		}
		if(Gdx.input.isKeyPressed(Keys.LEFT)) {
			player.isRunning = true;
			player.velocity.x =-Player.WALKING_SPEED; 
			player.state =Player.State.RUN;
		}
		else if (Gdx.input.isKeyPressed(Keys.RIGHT)){
			player.isRunning = true;
			player.velocity.x =Player.WALKING_SPEED; 
			player.state =Player.State.RUN;
		}
	}

}
