package com.mygdx.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.mygdx.game.Juego;
import com.mygdx.managers.ResourceManager;
import com.mygdx.managers.SpriteManager;
import com.mygdx.scenes.Hud;
public class GameScreen implements Screen{
	final Juego game;
	public ResourceManager resourceManager;
	public SpriteManager spriteManager;
	private Hud hud;
	
	public GameScreen(Juego game) {
		this.game = game;
		this.resourceManager = new ResourceManager();
		ResourceManager.loadAllResources();
		while(!ResourceManager.update()) {}
		spriteManager = new SpriteManager(game);
		hud = new Hud(game.batch);
	}
	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render(float delta) {
		spriteManager.update(delta);
		// colocar a negro
		Gdx.gl.glClearColor(0, 0, 0, 1);
		// limpiar el buffer
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		spriteManager.draw();
		// handleKeyBoard();
		hud.stage.draw();
		
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}
	

}
