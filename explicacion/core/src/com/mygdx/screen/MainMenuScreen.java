package com.mygdx.screen;



import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mygdx.game.Juego;


public class MainMenuScreen implements Screen {

	Juego game;
	Stage stage;
	
	public MainMenuScreen(Juego game) {
		this.game = game;
	}

	@Override
	public void show() {

        stage = new Stage();

        Table table = new Table();
        table.setFillParent(true);
        table.center();

        Label title = new Label("MENU", game.getSkin());
        title.setFontScale(2.5f);

        TextButton quickButton = new TextButton("Jugar", game.getSkin());
        quickButton.addListener(new ClickListener() {
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                dispose();
                game.setScreen(new GameScreen(game));
            }
        });
        
        TextButton optionsButton = new TextButton("Configuracion", game.getSkin());
        optionsButton.addListener(new ClickListener() {
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                dispose();
                game.setScreen(new ConfigurationScreen(game));

            }
        });
        TextButton exitButton = new TextButton("Salir", game.getSkin());
        exitButton.addListener(new ClickListener() {
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                dispose();
                System.exit(0);
            }
        });

        

        table.row().height(150);
        table.add(title).center().pad(35f);
        table.row().height(70);
        table.add(quickButton).center().width(300).pad(5f);
        table.row().height(70);
        table.add(optionsButton).center().width(300).pad(5f);
        table.row().height(70);
        table.add(exitButton).center().width(300).pad(5f);
        table.row().height(70);

        stage.addActor(table);
        Gdx.input.setInputProcessor(stage);
	}
	
	@Override
	public void render(float dt) {

		Gdx.gl.glClearColor(0, 0, 0.2f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        
		// Pinta el menú
		stage.act(dt);
		stage.draw();
	}
	
	@Override
	public void dispose() {

		stage.dispose();
	}

	@Override
	public void hide() {

		
	}

	@Override
	public void pause() {

		
	}

	

	@Override
	public void resize(int width, int height) {
        //stage.setViewport(width, height);
	}

	@Override
	public void resume() {
	}
}
