package com.mygdx.screen;



import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mygdx.game.Juego;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;


public class ConfigurationScreen implements Screen {

	Juego game;
	Stage stage;
	// Almacena las preferencias (en %UserProfile%/.prefs/PreferencesName)
	Preferences prefs;
	
	public ConfigurationScreen(Juego game) {
		this.game = game;
	}

    
	private void loadScreen() {

        stage = new Stage();

        Table table = new Table(game.getSkin());
        table.setFillParent(true);
        table.center();

        Label title = new Label("CONFIGURACION", game.getSkin());
        title.setFontScale(2.5f);

        final CheckBox checkSound = new CheckBox("SILENCIAR AUDIO", game.getSkin());
        
        checkSound.addListener(new ClickListener() {
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
               checkSound.setChecked(true);
            }
        });

        Label dificultad = new Label(" DIFICULTAD DE LA PARTIDA ", game.getSkin());

        String[] resolutionsArray = {"Baja", "Media", "Alta"};
        final List difficultyList = new List(game.getSkin());
        difficultyList.setItems(resolutionsArray);

        Label brillo = new Label(" NIVEL DE BRILLO ", game.getSkin());

        String[] brilloArray = {"Bajo", "Medio", "Alto"};
        final List brilloList = new List(game.getSkin());
        brilloList.setItems(brilloArray);



        TextButton exitButton = new TextButton("MENU", game.getSkin());
        exitButton.addListener(new ClickListener() {
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
              
                dispose();
                game.setScreen(new MainMenuScreen(game));
            }
        });


        table.add(title).center().pad(5f);
        
        table.row().height(20);
        table.add(dificultad).center().pad(10f);
        table.row().height(70);
        table.add(difficultyList).center().pad(5f);
        table.row().height(20);
        
        
        table.add(brillo).center().pad(5f);
        table.row().height(70);
        table.add(brilloList).center().pad(5f);
        table.row().height(70);
        
        table.row().height(70);
        table.add(checkSound).center().pad(5f);
        table.row().height(70);
        table.add(exitButton).center().width(300).pad(5f);
       

        stage.addActor(table);
        Gdx.input.setInputProcessor(stage);
	}
	

	
	@Override
	public void show() {

		loadScreen();
	}
	
	@Override
	public void render(float dt) {
	
		Gdx.gl.glClearColor(0, 0, 0.2f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        
		stage.act(dt);
		stage.draw();
	}

	@Override
	public void dispose() {
	
		stage.dispose();
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resize(int width, int height) {
        //stage.setViewport(width, height);
	}

	@Override
	public void resume() {
	}
}
