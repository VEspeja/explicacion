package com.mygdx.characters;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.managers.SpriteManager;


public abstract class Character {
	public Vector2 position;
	public Vector2 velocity;
	public int WIDTH;
	public int HEIGHT;
	public TextureRegion currentFrame;
	public float stateTime;
	public Rectangle rect;
	public boolean dead;
	public SpriteManager spriteManager;
	
	public Character(SpriteManager spriteManager) {
		this.spriteManager = spriteManager;
		this.dead = false;
		
	}
	
	public void render(Batch batch) {
		batch.draw(currentFrame, position.x, position.y,WIDTH, HEIGHT);
	}
	
	public abstract void update(float dt);
	public abstract void checkCollisions(SpriteManager spriteManager);
	public boolean isDead() {
		return dead;
	}

}
