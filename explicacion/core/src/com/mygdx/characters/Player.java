package com.mygdx.characters;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.managers.ResourceManager;
import com.mygdx.managers.SpriteManager;

public class Player extends Character{

	public boolean isJumping;
	public boolean canJump;
	public boolean isRunning;
	public State state;
	
	// crear una por cada animacion que va a realizar
	private TextureRegion walk;
	private TextureRegion run;
	private TextureRegion idle;
	private TextureRegion currentFrame;
	
	
	private Animation<TextureRegion> walkAnimation;
	private Animation<TextureRegion> runAnimation;
	private Animation<TextureRegion> idleAnimation;
	
	
	public enum State{
		IDLE,WALK,RUN
	}
	
	public static float WALKING_SPEED = 70.0f;
	public static float JUMPING_SPEED = 250.0f;
	public static float GRAVITY = 9f;
	
	
	
	public Player(SpriteManager spriteManager) {
		super(spriteManager);
		position = new Vector2();
		velocity = new Vector2();
		state = State.IDLE;
		
		TextureAtlas atlas = ResourceManager.assets.get("characters/Paco.pack",TextureAtlas.class);
		walk = atlas.findRegion("walkwalk");
		WIDTH = walk.getRegionWidth();// se puede dividir el tama�o en caso de que sea grande con '\3' o lo necesario
		HEIGHT = walk.getRegionHeight();
		
		walkAnimation = new Animation<TextureRegion>(0.15f,atlas.findRegions("walkwalk"));
		this.rect = new Rectangle(this.position.x, this.position.y,WIDTH,HEIGHT);
	}
	public void render(Batch spriteBach) {
		//sumar tiempo que se ha hecho el ultimo renderer
		stateTime += Gdx.graphics.getDeltaTime();
		
		//switch(state) {
		//case IDLE:
			//currentFrame = idleAnimation.getKeyFrame(stateTime,true);
			//break;
		//case RUN:
			//currentFrame = runAnimation.getKeyFrame(stateTime,true);
			//break;
		//case WALK:
			currentFrame = walkAnimation.getKeyFrame(stateTime,true);
			//break;
		//}
			
		if(this.velocity.x > 0) {
			spriteBach.draw(currentFrame, position.x+WIDTH, position.y, -WIDTH,HEIGHT);
		}else {
			spriteBach.draw(currentFrame, position.x, position.y, WIDTH,HEIGHT);
		}
	}
	
	public void update(float dt) {
		this.rect.setPosition(this.position);
		
		if(this.isJumping) {
			velocity.y = JUMPING_SPEED;
			this.isJumping = false;
		}
		velocity.y-=GRAVITY;
		
		if(velocity.y<-JUMPING_SPEED) {
			velocity.y = -JUMPING_SPEED;
		}
		this.checkCollisions(spriteManager);
		this.position.add(this.velocity.x*dt,this.velocity.y*dt);
		
	}
	
	public void jump() {
		this.isJumping = true;
		this.canJump = false;
	}
	@Override
	public void checkCollisions(SpriteManager spriteManager) {
		for (RectangleMapObject platform : spriteManager.platforms) {
			if(this.rect.overlaps(platform.getRectangle())&& this.velocity.y<0){
				this.velocity.set(new Vector2(this.velocity.x,0));
				this.canJump= true;
			}
		}
		
		
	}
	

	
	
	
	
}
